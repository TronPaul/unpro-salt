base:
  'roles:salt_master':
    - match: grain
    - salt-master
  'roles:vpn_server':
    - match: grain
    - unpro-openvpn
    - bind
    - bind.config
  'roles:aws_nat':
    - match: grain
    - aws.nat
  'roles:voice_server':
    - match: grain
    - mumble_servers
  'roles:sshserver':
    - match: grain
    - openssh
    - openssh.config
  'roles:nas':
    - match: grain
    - samba
    - unpro-nfs.server
  'roles:home-assistant':
    - match: grain
    - unpro-home-assistant
  'roles:torrent_server':
    - match: grain
    - unpro-deluge
  'roles:htpc':
    - match: grain
    - xbmc
    - pcsx2
    - desmume
    - dolphin
    - mupen64plus
  'roles:nas_client':
    - match: grain
    - nfs.client
  'G@roles:voice_server and G@ec2:instance_id':
    - match: compound
    - mumble_servers.database
  'G@roles:vpn_server and G@ec2:instance_id':
    - match: compound
    - unpro-openvpn.files
