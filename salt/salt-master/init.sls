/etc/salt/master:
  file.managed:
    - user: root
    - group: root
    - source: salt['pillar.get']('salt-master:master_source', 'salt://salt-master/master')
