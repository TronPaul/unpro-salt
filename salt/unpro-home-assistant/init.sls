{%- from "home-assistant/map.jinja" import home_assistant with context %}
include:
  - home-assistant

{{ home_assistant.venv }}/lib/python3.5/site-packages/libopenzwave-0.3.1-py3.5-linux-x86_64.egg/config/options.xml:
  file.blockreplace:
    - marker_start:   <Option name="logging" value="true" />
    - marker_end:   <Option name="Associate" value="true" />
    - content:   <Option name="LogFileName" value="/var/log/hass/OZW_Log.log" />
    - require:
      - cmd: install-zwave

{{ home_assistant.venv }}/lib/python3.5/site-packages/libopenzwave-0.3.1-py3.5-linux-x86_64.egg/config/linear/TBZ48-1.xml:
  file.managed:
    - source: salt://unpro-home-assistant/TBZ48-1.xml
    - user: hass
    - group: nogroup
    - require:
      - cmd: install-zwave

{{ home_assistant.venv }}/lib/python3.5/site-packages/libopenzwave-0.3.1-py3.5-linux-x86_64.egg/config/manufacturer_specific.xml:
  file.managed:
    - source: salt://unpro-home-assistant/manufacturer_specific.xml
    - user: hass
    - group: nogroup
    - require:
      - cmd: install-zwave
