"""
Module with location helpers.

detect_location_info and elevation are mocked by default during tests.
"""
import collections
import requests


ELEVATION_URL = 'http://maps.googleapis.com/maps/api/elevation/json'
FREEGEO_API = 'https://freegeoip.io/json/'
IP_API = 'http://ip-api.com/json'

LocationInfo = collections.namedtuple(
    "LocationInfo",
    ['ip', 'country_code', 'country_name', 'region_code', 'region_name',
     'city', 'zip_code', 'time_zone', 'latitude', 'longitude',
     'use_metric'])


def geodata():
    loc_info = _detect_location_info()
    elevation = _elevation(loc_info.latitude, loc_info.longitude)
    return dict(geodata=dict(latitude=loc_info.latitude, longitude=loc_info.longitude,
            elevation=elevation, use_metric=loc_info.use_metric,
            time_zone=loc_info.time_zone))


def _detect_location_info():
    """Detect location information."""
    data = _get_freegeoip()

    if data is None:
        data = _get_ip_api()

    if data is None:
        return None

    data['use_metric'] = data['country_code'] not in (
        'US', 'MM', 'LR')

    return LocationInfo(**data)


def _elevation(latitude, longitude):
    """Return elevation for given latitude and longitude."""
    try:
        req = requests.get(
            ELEVATION_URL,
            params={
                'locations': '{},{}'.format(latitude, longitude),
                'sensor': 'false',
            },
            timeout=10)
    except requests.RequestException:
        return 0

    if req.status_code != 200:
        return 0

    try:
        return int(float(req.json()['results'][0]['elevation']))
    except (ValueError, KeyError):
        return 0


def _get_freegeoip():
    """Query freegeoip.io for location data."""
    try:
        raw_info = requests.get(FREEGEO_API, timeout=5).json()
    except (requests.RequestException, ValueError):
        return None

    return {
        'ip': raw_info.get('ip'),
        'country_code': raw_info.get('country_code'),
        'country_name': raw_info.get('country_name'),
        'region_code': raw_info.get('region_code'),
        'region_name': raw_info.get('region_name'),
        'city': raw_info.get('city'),
        'zip_code': raw_info.get('zip_code'),
        'time_zone': raw_info.get('time_zone'),
        'latitude': raw_info.get('latitude'),
        'longitude': raw_info.get('longitude'),
    }


def _get_ip_api():
    """Query ip-api.com for location data."""
    try:
        raw_info = requests.get(IP_API, timeout=5).json()
    except (requests.RequestException, ValueError):
        return None

    return {
        'ip': raw_info.get('query'),
        'country_code': raw_info.get('countryCode'),
        'country_name': raw_info.get('country'),
        'region_code': raw_info.get('region'),
        'region_name': raw_info.get('regionName'),
        'city': raw_info.get('city'),
        'zip_code': raw_info.get('zip'),
        'time_zone': raw_info.get('timezone'),
        'latitude': raw_info.get('lat'),
        'longitude': raw_info.get('lon'),
    }
